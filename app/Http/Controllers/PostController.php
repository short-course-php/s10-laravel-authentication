<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//These ones are added
use Illuminate\Support\Facades\Auth; //To check if the user is Authenticated/Loggedin and you can also get their id
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create(){
        //post.create means post folder then create.blade.php file in resources
        return view('posts.create');
    }

    public function store(Request $request){
        if(Auth::user()) {
            //create a new Post object from the Post model
            $post = new Post;

            //title & content is in the "name" in the post from not the "id" but to be sure just put the same value in name & id in the input field
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);
            //save the post object to the database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }  
    }

    public function index(){        
        $posts = Post::where('isActive', true)->get(); //get all ACTIVE posts from the database
        return view('posts.index')->with('posts', $posts);
    }

    public function featured(){
        //get all posts from the database
        $posts = Post::latest()->take(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    public function myPosts(){
        if(Auth::user()) {
            $posts = Auth::user()->posts; //retrieve the user's own posts

            return view('posts.index')->with('posts', $posts); //recycled the posts.index
        }else {
            return redirect('/login');
        }
    }

    //parameter $varName must be the same as in the router file (web.php)
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    public function editView($id){
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    //Pass both the form data in the request, as well as the id of the post to be updated
    public function update(Request $request, $id){
        $post = Post::find($id);

        //If authenticated user's id is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }

        return redirect('/posts');
    }
    //No need for delete, we'll archive instead
    /* public function delete($id){
        $post = Post::find($id);

        //If authenticated user's id is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }

        return redirect('/posts');

    } */

    public function archive($id){
        $post = Post::find($id);

        //If authenticated user's id is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }

        return redirect('/posts');
    }

    /* public function unarchive($id){
        $post = Post::find($id);

        //If authenticated user's id is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->isActive = true;
            $post->save();
        }

        return redirect('/posts');
    } */

    //Import the PostLike model at the top of the code
    public function like($id){
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        //check if the authenticated user is NOT the post author
        if($post->user_id != $user_id){
            //check if a post has already been liked by this user
            if($post->likes->contains("user_id", $user_id)){
                //delete the like made by the user to unlike the post
                PostLike::where("post_id", $post->id)->where("user_id", $user_id)->delete();
            }else{
                //create a new postLike object
                $postLike = new PostLike;

                //define the properties of the object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            return redirect("/posts/$id");
        }
    }

    public function comment(Request $request, $id){
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if(Auth::user()){
            $comment = new PostComment;

            $comment->post_id = $post->id;
            $comment->user_id = $user_id;
            $comment->content = $request->input('content');

            $comment->save();
            return redirect("/posts/$id");
        }else{
            return redirect("/login");
        }
        
    }       

}
