<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    //This one is modified
    public function index()
    {

        $topics = ['HTML', 'CSS', 'JavaScript', 'SQL', 'PHP'];
        return view('home')->with('topics', $topics); //with() is to pass data
    }
}
