@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Likes: {{count($post->likes)}} </p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				</form>
			@endif
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
			  Leave Comments
			</button>

			
			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>

	<h3 class="pt-5">Comments</h3>
	@if(count($post->comments) > 0)
		@foreach($post->comments as $comment)
			<div class="my-3">
				<h5 class="d-inline">{{$comment->user->name}}:</h5>
				<p class="d-inline">{{$comment->content}}</p>
			</div>
		@endforeach
	@else
		<div>
	        <h2>There are no comments for this post</h2>
	    </div>
	@endif

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Leave a Comment</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	            <form method="POST" action="/posts/{{$post->id}}/comment">
	              {{-- @csrf to avoid csrf error or 419 Page Expired --}}
	              @csrf
	              
	        		<div class="form-group">
		                <label for="content">Comment:</label>
		                <textarea class="form-control" id="content" name="content" rows="3"></textarea>
	              	</div>
	              	<div class="mt-2">
	              		<button type="submit" class="btn btn-primary">Reply</button>
	          	  	</div>
	            </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
@endsection