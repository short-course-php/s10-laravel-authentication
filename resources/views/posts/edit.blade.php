@extends('layouts.app')

@section('content')
	<h1 class="text-center">EDIT POST</h1>
	<div class="d-flex justify-content-center">
		<form method="POST" action="/posts/{{$post->id}}/edit" style="width: 500px; border: 1px solid grey; padding: 20px;">
			@method('PUT')
	      	@csrf
	    	<div class="form-group" >
		        <label for="title">Title: </label>
		        <input type="text" class="form-control" id="title" name="title" value="{{old('title', $post->title)}}">
	      	</div>
			<div class="form-group">
		        <label for="content">Content:</label>
		        <textarea class="form-control" id="content" name="content" rows="5">{{old('content', $post->content)}}</textarea>
	      	</div>
	      	<div class="mt-2">
	      		<button type="submit" class="btn btn-primary">Edit Post</button>
	  	  	</div>
	    </form>
    </div>
@endsection