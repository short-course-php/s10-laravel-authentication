{{-- layouts.app basically means App.js in react and it's located in app file inside layouts folder in resources --}}
@extends('layouts.app')

{{-- content means in the layouts.app and in the bottom and there's a @yield('content') there --}}
@section('content')
    <form method="POST" action="/posts">
      {{-- @csrf to avoid csrf error or 419 Page Expired --}}
      @csrf
      <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" class="form-control" id="title" name="title">
      </div>
<div class="form-group">
        <label for="content">Content:</label>
        <textarea class="form-control" id="content" name="content" rows="3"></textarea>
      </div>
      <div class="mt-2">
      	<button type="submit" class="btn btn-primary">Create Post</button>
  	  </div>
    </form>
@endsection