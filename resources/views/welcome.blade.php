@extends('layouts.app')

@section('content')
    <style type="text/css">
        .card{width: 300px; border-radius: 20px;}
        .card:hover{border: 3px solid black; box-shadow: 2px 5px #888888;}
        .postContainer{border: 1px solid #000; border-radius: 15px 50px;}
        .postContainer h2{text-align: center;}
    </style>
    <div class="text-center">
        <img src="https://surround-bg.com/wp-content/uploads/2018/10/laravel-logo.png" alt="Laravel Logo" style="height: 250px;">
    </div>
    <div class="postContainer pt-3">
        <h2>Latest Posts </h2>
        <div class="d-flex flex-column flex-sm-row justify-content-evenly align-items-center flex-wrap pb-5">
        @foreach($posts as $post)
        
            <div class="card text-center mt-3">
                <div class="card-body" >
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                </div>
            </div>
        
        @endforeach
        </div>
    </div>
@endsection