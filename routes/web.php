<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// }); 

Auth::routes();

/* This code is the default from laravel
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Effecient way to do is import/use it at the top of the code so that you will not type the path anymore
*/
//'index' is a function so it means that find the index function
Route::get('/home', [HomeController::class, 'index']);

//New Features
    //Return a view where the user can create a post
    Route::get('/', [PostController::class, 'featured']);
    Route::get('/posts/create', [PostController::class, 'create']);
    //route for a route wherein form data can be sent via POST method
    Route::post('/posts', [PostController::class, 'store']);
    //Return a view containing all posts
    Route::get('/posts', [PostController::class, 'index']);
    //Return a view containing only the autenticated user's posts
    Route::get('/posts/myPosts', [PostController::class, 'myPosts']);
    //Return specific posts view
    Route::get('/posts/{id}', [PostController::class, 'show']);
    //Edit Post Form View
    Route::get('/posts/{id}/edit', [PostController::class, 'editView']);
    //Update Post function
    Route::put('/posts/{id}', [PostController::class, 'update']);
    //Delete Post function (No need for this because we will archive it instead)
    //Route::delete('/posts/{id}', [PostController::class, 'delete']);
    //Archive Post function
    Route::delete("/posts/{id}", [PostController::class, 'archive']);
    //Route::put('/posts/{id}', [PostController::class, 'unarchive']);
    //Route that will enable users to like/unlike posts
    Route::put('/posts/{id}/like', [PostController::class, 'like']);
    //Route that enable users to leave a comment
    Route::post('/posts/{id}/comment', [PostController::class, 'comment']);
